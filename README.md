# ByteSweep

A Free Software IoT Firmware Security Analysis Tool

## Install

### Kali Linux

1. Install System Dependencies
```
apt-get install radare2 python3 python3-pip
```

2. Clone Repo
```
git clone https://gitlab.com/bytesweep/bytesweep.git
```

3. Install Python Dependencies
```
cd bytesweep
pip3 install -r requirements.txt
```

4. Install ByteSweep via pip
```
pip3 install .
```

## Usage
```
usage: bytesweep [-h] [-oN OUTPUT_NORMAL] [-oJ OUTPUT_JSON] [-oP OUTPUT_PDF] [-D EXTRACTION_DIRECTORY] [-j JOBS] [-v] file

positional arguments:
  file                  File to analyize

optional arguments:
  -h, --help            show this help message and exit
  -oN OUTPUT_NORMAL, --output-normal OUTPUT_NORMAL
                        Output results in normal format
  -oJ OUTPUT_JSON, --output-json OUTPUT_JSON
                        Output results in json format
  -oP OUTPUT_PDF, --output-pdf OUTPUT_PDF
                        Output results in pdf format
  -D EXTRACTION_DIRECTORY, --extraction-directory EXTRACTION_DIRECTORY
                        Extraction directory
  -j JOBS, --jobs JOBS  Number of processes to create when multiprocessing
  -v, --verbose         Verbose output

```
