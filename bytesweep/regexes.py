
regexes = [
    {
        "regex": {
            "string": "mke2fs %s \\(%s\\)",
            "name": "mke2fs",
            "type": "program version",
            "description": "mke2fs version",
            "nearby": {
                "string": "[0-9]+\\.[0-9]+\\.[0-9]+",
                "name": "mke2fs",
                "type": "program version",
                "description": "mke2fs version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "%s/%s",
            "name": "openssl",
            "vendor": "openssl",
            "type": "program version",
            "description": "openssl version",
            "nearby": {
                "string": "OpenSSL [0-9]+\\.[0-9]+\\.[0-9a-z]+",
                "name": "openssl",
                "vendor": "openssl",
                "type": "program version",
                "description": "openssl version",
                "search_above": 10,
                "search_below": 5,
                "left_nibble": 8,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "%s \\(Library: %s\\)",
            "name": "openssl",
            "vendor": "openssl",
            "type": "program version",
            "description": "openssl version",
            "nearby": {
                "string": "OpenSSL [0-9]+\\.[0-9]+\\.[0-9a-z]+",
                "name": "openssl",
                "vendor": "openssl",
                "type": "program version",
                "description": "openssl version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 8,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "General [a-zA-Z\\s]*Options:",
            "name": "openvpn",
            "type": "program version",
            "description": "openvpn version",
            "nearby": {
                "string": "OpenVPN [0-9]+\\.[0-9]+\\.[0-9]+ ",
                "name": "openvpn",
                "type": "program version",
                "description": "openvpn version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 8,
                "right_nibble": 1
            }
        }
    },
    {
        "regex": {
            "string": "stunnel [0-9]+\\.[0-9]+ on",
            "name": "stunnel",
            "type": "program version",
            "description": "stunnel version",
            "left_nibble": 8,
            "right_nibble": 3
        }
    },
    {
        "regex": {
            "string": "wpa_supplicant v[0-9]+\\.[0-9]+",
            "name": "wpa_supplicant",
            "type": "program version",
            "description": "wpa_supplicant version",
            "left_nibble": 16,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "wpa_cli v[0-9]+\\.[0-9]+",
            "name": "wpa_cli",
            "type": "program version",
            "description": "wpa_cli version",
            "left_nibble": 9,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "dnsmasq-[0-9]+\\.[0-9]+",
            "name": "dnsmasq",
            "type": "program version",
            "description": "dnsmasq version",
            "left_nibble": 8,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "BusyBox v[0-9]+\\.[0-9]+\\.[0-9]+ ",
            "name": "busybox",
            "type": "program version",
            "description": "busybox version",
            "left_nibble": 9,
            "right_nibble": 1
        }
    },
    {
        "regex": {
            "string": "iperf3 homepage at: http://software.es.net/iperf/",
            "name": "iperf3",
            "type": "program version",
            "description": "iperf3 version",
            "nearby": {
                "string": "iperf [0-9]+\\.[0-9]+\\.[0-9]+",
                "name": "iperf3",
                "type": "program version",
                "description": "iperf3 version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 6,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "Server listening on %s port %d",
            "name": "iperf2",
            "type": "program version",
            "description": "iperf2 version",
            "nearby": {
                "string": "iperf version [0-9]+\\.[0-9]+\\.[0-9]+",
                "name": "iperf2",
                "type": "program version",
                "description": "iperf2 version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 14,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "GNU Parted [0-9]+\\.[0-9]+",
            "name": "parted",
            "type": "program version",
            "description": "parted version",
            "left_nibble": 11,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "pppd %s started by %s, uid %d",
            "name": "pppd",
            "type": "program version",
            "description": "pppd version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+\\.[0-9]+$",
                "name": "pppd",
                "type": "program version",
                "description": "pppd version",
                "search_above": 3,
                "search_below": 3,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "GNU Parted [0-9]+\\.[0-9]+",
            "name": "parted",
            "type": "program version",
            "description": "parted version",
            "left_nibble": 11,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "upnpc : miniupnpc library test client",
            "name": "upnpc",
            "type": "program version",
            "description": "upnpc version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+$",
                "name": "upnpc",
                "type": "program version",
                "description": "upnpc version",
                "search_above": 3,
                "search_below": 3,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "# Generated by iptables-save v%s on %s",
            "name": "iptables",
            "type": "program version",
            "description": "iptables version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+\\.[0-9]+$",
                "name": "iptables",
                "type": "program version",
                "description": "iptables version",
                "search_above": 3,
                "search_below": 3,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "ethtool version [0-9]+\\.*[0-9]*",
            "name": "ethtool",
            "type": "program version",
            "description": "ethtool version",
            "left_nibble": 16,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "hcitool - HCI Tool ver %s",
            "name": "hcitool",
            "type": "program version",
            "description": "hcitool version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+$",
                "name": "hcitool",
                "type": "program version",
                "description": "hcitool version",
                "search_above": 3,
                "search_below": 3,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "^hostapd v[0-9]+\\.[0-9]+\\.*[0-9]*",
            "name": "hostapd",
            "type": "program version",
            "description": "hostapd version",
            "left_nibble": 9,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "Notify the kernel about the current regulatory domain\\.",
            "name": "iw",
            "type": "program version",
            "description": "iw version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+\\.*[0-9]*$",
                "name": "iw",
                "type": "program version",
                "description": "iw version",
                "search_above": 3,
                "search_below": 3,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "version [0-9]+\\.[0-9]+ starting%s%sext if %s BOOTID=%u",
            "name": "miniupnpd",
            "type": "program version",
            "description": "miniupnpd version",
            "left_nibble": 8,
            "right_nibble": 32
        }
    },
    {
        "regex": {
            "string": "usage: sshd ",
            "name": "openssh-server",
            "type": "program version",
            "description": "openssh-server version",
            "nearby": {
                "string": "^OpenSSH_[0-9+]\\.[0-9a-z]+$",
                "name": "openssh-server",
                "type": "program version",
                "description": "openssh-server version",
                "search_above": 3,
                "search_below": 3,
                "left_nibble": 8,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "^Invalid -J argument$",
            "name": "openssh-client",
            "type": "program version",
            "description": "openssh-client version",
            "nearby": {
                "string": "^OpenSSH_[0-9+]\\.[0-9a-z]+$",
                "name": "openssh-server",
                "type": "program version",
                "description": "openssh-server version",
                "search_above": 3,
                "search_below": 3,
                "left_nibble": 8,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "Email:    ucd-snmp-coders@ucd-snmp\\.ucdavis\\.edu",
            "name": "snmpd",
            "type": "program version",
            "description": "snmpd version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+\\.*[0-9]*$",
                "name": "snmpd",
                "type": "program version",
                "description": "snmpd version",
                "search_above": 7,
                "search_below": 5,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "udhcpcd, version %s",
            "name": "udhcpc",
            "type": "program version",
            "description": "udhcpc version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+\\.*[0-9a-z\\-]*$",
                "name": "udhcpc",
                "type": "program version",
                "description": "udhcpc version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "%s \\(v%s\\) started",
            "name": "udhcpd",
            "type": "program version",
            "description": "udhcpd version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+\\.*[0-9a-z\\-]*$",
                "name": "udhcpd",
                "type": "program version",
                "description": "udhcpd version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "Usage: pptpd \\[options\\], where options are:",
            "name": "pptpd",
            "type": "program version",
            "description": "pptpd version",
            "nearby": {
                "string": "^[0-9]+\\.[0-9]+\\.[0-9]+$",
                "name": "pptpd",
                "type": "program version",
                "description": "pptpd version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "Usage: curl ",
            "name": "curl",
            "type": "program version",
            "description": "curl version",
            "nearby": {
                "string": "^curl [0-9]+\\.[0-9]+\\.[0-9]+ ",
                "name": "curl",
                "type": "program version",
                "description": "curl version",
                "search_above": 5,
                "search_below": 5,
                "left_nibble": 5,
                "right_nibble": 1
            }
        }
    },
    {
        "regex": {
            "string": "GNU C Library \\(GNU libc\\) stable release version [0-9]+\\.[0-9]+",
            "name": "glibc",
            "type": "library version",
            "description": "glibc version",
            "left_nibble": 48,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "Enabled GnuTLS [0-9]+\\.[0-9]+\\.[0-9]+ logging",
            "name": "gnutls",
            "type": "library version",
            "description": "gnutls version",
            "left_nibble": 15,
            "right_nibble": 8
        }
    },
    {
        "regex": {
            "string": "mbed TLS [0-9]+\\.[0-9]+\\.[0-9]+",
            "name": "mbed_tls",
            "type": "library version",
            "description": "mbed_tls version",
            "left_nibble": 9,
            "right_nibble": 0
        }
    },
    {
        "regex": {
            "string": "-----BEGIN OPENSSH PRIVATE KEY-----",
            "name": "ssh-private-key",
            "type": "crypto-keys",
            "description": "OpenSSH Private Key",
            "multiline": {
                "string": "-----BEGIN OPENSSH PRIVATE KEY-----[a-zA-Z0-9+=/\\n]{20,}?-----END OPENSSH PRIVATE KEY-----",
                "name": "ssh-private-key",
                "type": "crypto-keys",
                "description": "OpenSSH Private Key",
                "search_above": 0,
                "search_below": 50,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "-----BEGIN RSA PRIVATE KEY-----",
            "name": "rsa-private-key",
            "type": "crypto-keys",
            "description": "RSA Private Key",
            "multiline": {
                "string": "-----BEGIN RSA PRIVATE KEY-----[a-zA-Z0-9+=/\\n]{20,}?-----END RSA PRIVATE KEY-----",
                "name": "rsa-private-key",
                "type": "crypto-keys",
                "description": "RSA Private Key",
                "search_above": 0,
                "search_below": 50,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "-----BEGIN CERTIFICATE-----",
            "name": "rsa-public-key",
            "type": "crypto-keys",
            "description": "RSA Public Key",
            "multiline": {
                "string": "-----BEGIN CERTIFICATE-----[a-zA-Z0-9+=/\\n]{20,}?-----END CERTIFICATE-----",
                "name": "rsa-public-key",
                "type": "crypto-keys",
                "description": "RSA Public Key",
                "search_above": 0,
                "search_below": 50,
                "left_nibble": 0,
                "right_nibble": 0
            }
        }
    },
    {
        "regex": {
            "string": "[0-9a-zA-Z]+:[$0-9a-zA-Z.\\/]{5,}:[0-9]*:[0-9]*:[0-9]*:[0-9]*:[0-9]*:[0-9]*:$",
            "name": "unix-shadow",
            "type": "password",
            "description": "unix password hash"
        }
    },
    {
        "regex": {
            "string": "[0-9a-zA-Z]+:[$0-9a-zA-Z.\\/]{5,}:[0-9]*:[0-9]*:[0-9a-zA-Z\\s]*:[0-9a-zA-Z.\\/\\-\\_]*:[0-9a-zA-Z.\\/\\-\\_]*$",
            "name": "unix-passwd",
            "type": "password",
            "description": "unix password hash"
        }
    }
]

