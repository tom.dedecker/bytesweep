#!/usr/bin/env python3
import re
import string
import time
from ruamel.yaml import YAML
from bytesweep.regexes import regexes

printables = []
for s in list(string.printable):
    printables.append(s.encode('utf-8'))
printable_chars = set(printables)

def get_strings(filename, threshold):
	strings = []
	s = ''
	parsing_str = False
	f = open(filename, 'rb')
	offset = 0
	first = True
	while True:
		c = f.read(1)
		if c == b"":
			break
		if c in printable_chars and c != b'\n':
			if not parsing_str and first:
				first = False
			elif not parsing_str:
				offset = f.tell()
			parsing_str = True
			s += c.decode('utf-8')
		elif parsing_str:
			if len(s) >= threshold:
				strings.append({'offset':offset,'string':s})
			s = ''
			parsing_str = False
	if len(s) >= threshold:
		strings.append({'offset':offset,'string':s})
	return strings

def compile_regex_exps(regex_list):
	compiled_regex_list = []
	for regex in regex_list:
		regex = regex['regex']
		if 'vendor' not in regex:
			regex['vendor'] = ''
		regex["compiled"] = re.compile(regex["string"])
		if 'nearby' in regex:
			nearby_regex = regex['nearby']
			if 'vendor' not in nearby_regex:
				nearby_regex['vendor'] = ''
			nearby_regex['compiled'] = re.compile(nearby_regex['string'])
		if 'multiline' in regex:
			multiline_regex = regex['multiline']
			if 'vendor' not in multiline_regex:
				multiline_regex['vendor'] = ''
			multiline_regex['compiled'] = re.compile(multiline_regex['string'],re.MULTILINE)


def regex_matcher(regex_list,string_list,f):
	matches = []
	for regex in regex_list:
		regex = regex['regex']
		for index,string_and_offset in enumerate(string_list):
			string = string_and_offset['string']
			offset = string_and_offset['offset']
			for match in regex['compiled'].findall(string):
				# TODO: break some of the dup code into functions
				if 'nearby' in regex:
					nearby_regex = regex['nearby']
					temp = [] # TODO loop through surounding strings
					left_index = index - nearby_regex['search_above']
					right_index = index + nearby_regex['search_below'] + 1
					# ensure left and right index don't go outside of the bounds of string_list
					if left_index < 0:
						left_index = 0
					if right_index > len(string_list):
						right_index = len(string_list)
					for nearby_string_and_offset in string_list[left_index:right_index]: # FIX THIS... WRONG if out of bounds.....
						nearby_string = nearby_string_and_offset['string']
						nearby_offset = nearby_string_and_offset['offset']
						for nearby_match in nearby_regex['compiled'].findall(nearby_string):
							left_nibble = 0
							right_nibble = 0
							if 'left_nibble' in nearby_regex and 'right_nibble' in nearby_regex:
								if nearby_regex['left_nibble'] + nearby_regex['right_nibble'] < len(nearby_match):
									left_nibble = nearby_regex['left_nibble']
									right_nibble = nearby_regex['right_nibble']
							elif 'left_nibble' in nearby_regex:
								if nearby_regex['left_nibble'] < len(nearby_match):
									left_nibble = nearby_regex['left_nibble']
							elif 'right_nibble' in nearby_regex:
								if nearby_regex['right_nibble'] < len(nearby_match):
									right_nibble = nearby_regex['right_nibble']
							nearby_match = nearby_match[left_nibble:len(nearby_match)-right_nibble]
							
							matches.append({'match':nearby_match,'string':nearby_string,'type':nearby_regex['type'],'offset':nearby_offset,'regex_name':nearby_regex['name'],'vendor':nearby_regex['vendor'],'regex_description':nearby_regex['description'],'path':f['path'],'relpath':f['relpath'],'fid':f['fid']})

				elif 'multiline' in regex:
					multiline_regex = regex['multiline']
					temp = [] # TODO loop through surounding strings
					left_index = index - multiline_regex['search_above']
					right_index = index + multiline_regex['search_below']
					# ensure left and right index don't go outside of the bounds of string_list
					if left_index < 0:
						left_index = 0
					if right_index > len(string_list):
						right_index = len(string_list)
					multiline_string = ''
					for multiline_string_and_offset in string_list[left_index:right_index]: # FIX THIS... WRONG if out of bounds.....
						multiline_string += multiline_string_and_offset['string']+'\n'

					for multiline_match in multiline_regex['compiled'].findall(multiline_string):
						left_nibble = 0
						right_nibble = 0
						if 'left_nibble' in multiline_regex and 'right_nibble' in multiline_regex:
							if multiline_regex['left_nibble'] + multiline_regex['right_nibble'] < len(multiline_match):
								left_nibble = multiline_regex['left_nibble']
								right_nibble = multiline_regex['right_nibble']
						elif 'left_nibble' in multiline_regex:
							if multiline_regex['left_nibble'] < len(multiline_match):
								left_nibble = multiline_regex['left_nibble']
						elif 'right_nibble' in multiline_regex:
							if multiline_regex['right_nibble'] < len(multiline_match):
								right_nibble = multiline_regex['right_nibble']
						multiline_match = multiline_match[left_nibble:len(multiline_match)-right_nibble]
						
						matches.append({'match':multiline_match,'string':multiline_string,'type':multiline_regex['type'],'offset':offset,'regex_name':multiline_regex['name'],'vendor':multiline_regex['vendor'],'regex_description':multiline_regex['description'],'path':f['path'],'relpath':f['relpath'],'fid':f['fid']})

				else:
					left_nibble = 0
					right_nibble = 0
					if 'left_nibble' in regex and 'right_nibble' in regex:
						if regex['left_nibble'] + regex['right_nibble'] < len(match):
							left_nibble = regex['left_nibble']
							right_nibble = regex['right_nibble']
					elif 'left_nibble' in regex:
						if regex['left_nibble'] < len(match):
							left_nibble = regex['left_nibble']
					elif 'right_nibble' in regex:
						if regex['right_nibble'] < len(match):
							right_nibble = regex['right_nibble']
					match = match[left_nibble:len(match)-right_nibble]
					matches.append({'match':match,'string':string,'type':regex['type'],'offset':offset,'regex_name':regex['name'],'vendor':regex['vendor'],'regex_description':regex['description'],'path':f['path'],'relpath':f['relpath'],'fid':f['fid']})
	return matches

def string_analysis(f_list,threshold):
	regex_matches = []
	regex_list = regexes
	compile_regex_exps(regex_list)
	for f in f_list:
		f = f['node']
		if f["islink"] or f["isdir"] or not f["isfile"]:
			continue
		string_list = get_strings(f['path'],threshold)
		regex_matches += regex_matcher(regex_list,string_list,f)
	i = 0
	for regex_match in regex_matches:
		regex_match['match_id'] = i
		i += 1
	return regex_matches
