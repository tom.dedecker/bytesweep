#!/usr/bin/env python3
import os
import stat
import magic
import hashlib
import math

executable_mime_types = ['application/x-executable','application/x-pie-executable','application/x-sharedlib']

def is_executable(f):
	mimemagic = magic.from_file(f['path'], mime=True)
	return mimemagic in executable_mime_types

def sha256_hash(f):
	BLOCKSIZE = 65536
	hasher = hashlib.sha256()
	with open(f, 'rb') as afile:
		buf = afile.read(BLOCKSIZE)
		while len(buf) > 0:
			hasher.update(buf)
			buf = afile.read(BLOCKSIZE)
	return hasher.hexdigest()

def md5_hash(f):
	BLOCKSIZE = 65536
	hasher = hashlib.md5()
	with open(f, 'rb') as afile:
		buf = afile.read(BLOCKSIZE)
		while len(buf) > 0:
			hasher.update(buf)
			buf = afile.read(BLOCKSIZE)
	return hasher.hexdigest()

def sha1_hash(f):
	BLOCKSIZE = 65536
	hasher = hashlib.sha1()
	with open(f, 'rb') as afile:
		buf = afile.read(BLOCKSIZE)
		while len(buf) > 0:
			hasher.update(buf)
			buf = afile.read(BLOCKSIZE)
	return hasher.hexdigest()

# TODO: remove in a bit. this isn't used anymore
#def enrich_files(files):
#	for f in files:
#		# determine path type
#		files[f]['isfile'] = os.path.isfile(f)
#		files[f]['isdir'] = os.path.isdir(f)
#		files[f]['islink'] = os.path.islink(f)
#
#		# the follow enrichments only apply to files
#		if files[f]['isfile'] and not files[f]['isdir'] and not files[f]['islink']:
#			# determine magic
#			m = magic.detect_from_filename(f)
#			files[f]['magic'] = {}
#			files[f]['magic']['encoding'] = m.encoding
#			files[f]['magic']['mime_type'] = m.mime_type
#			files[f]['magic']['fulltext'] = m.name
#			
#			# compute sha256 hash
#			files[f]['hash'] = {}
#			files[f]['hash']['sha256'] = sha256_hash(f)

# this is called from the binwalk module
def enrich_file(f):

	# determine path type
	
	f['isfile'] = os.path.isfile(f['path'])
	f['isdir'] = os.path.isdir(f['path'])
	f['islink'] = os.path.islink(f['path'])

	# the follow enrichments only apply to files
	if f['isfile'] and not f['isdir'] and not f['islink']:
		# make file readable if it isn't already
		if not os.stat(f['path']).st_mode & stat.S_IRUSR:
			os.chmod(f['path'],os.stat(f['path']).st_mode | stat.S_IRUSR)
		f['isregfile'] = stat.S_ISREG(os.stat(f['path']).st_mode)
		f['size'] = os.path.getsize(f['path'])

		# determine magic
		fullmagic = magic.from_file(f['path'])
		mimemagic = magic.from_file(f['path'], mime=True)
		f['magic'] = {}
		f['magic']['mime_type'] = mimemagic
		f['magic']['fulltext'] = fullmagic
		
		# compute sha256 hash
		f['hash'] = {}
		f['hash']['md5'] = md5_hash(f['path'])
		f['hash']['sha1'] = sha1_hash(f['path'])
		f['hash']['sha256'] = sha256_hash(f['path'])

		# compute shannon entropy and Index of Coincidence
		f['byte-analysis'] = {}
		entropy_and_ioc = calc_entropy_and_ioc(f['path'])
		f['byte-analysis']['entropy'] = entropy_and_ioc['entropy']
		f['byte-analysis']['ioc'] = entropy_and_ioc['ioc']
	else:
		f['isregfile'] = False

def calc_entropy_and_ioc(filename):
	buckets = []
	total_bytes = 0.0
	for i in range(256):
		buckets.append(0)
	f = open(filename,'rb')
	while True:
		b = f.read(1)
		if b == b'':
			break
		i = ord(b)
		buckets[i] += 1
		total_bytes += 1
	f.close()
	
	# exit early if total_bytes is 0
	if total_bytes < 2:
		return {'ioc':0.0,'entropy':0.0}

	ioc = 0.0
	entropy = 0.0
	for i in range(256):
		frequency = float(buckets[i])
		# calculate IoC
		ioc += frequency*(frequency-1.0)
		# calculate entropy
		if frequency > 0:
			entropy = entropy + (frequency / total_bytes) * math.log((frequency / total_bytes), 2)
	ioc = ioc / (total_bytes*(total_bytes-1.0)) / 256.0
	entropy = -entropy / 8
	entropy = round(entropy,5)
	ioc = round(ioc,5)
	return {'ioc':ioc,'entropy':entropy}


